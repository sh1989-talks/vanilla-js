<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<title>Vanilla and Framework-less JS</title>

		<link rel="stylesheet" href="dist/reset.css">
		<link rel="stylesheet" href="dist/reveal.css">
		<link rel="stylesheet" href="dist/theme/white.css">
		<link rel="stylesheet" href="plugin/highlight/monokai.css">

		<style>
			.horizontal-list li {
				display: inline-block;
			}

			.tick-list li {
				list-style-type: none;
			}

			.side-by-side {
				display: flex;
			}

			.side-by-side pre {
				flex: 1;
				margin-right: 24px;
			}

			.side-by-side pre:last-child {
				margin-right: 0;
			}

			.side-by-side code {
				margin-bottom: 24px;
			}

			.side-by-side code:last-child {
				margin-bottom: 0;
			}

			.supplementary {
				color: #C99C3C;
			}
		</style>
	</head>
	<body>
		<template id="sealion">
			<aside class="sealion">
				<img src="assets/sealion.png">
				<slot name="text">Some text here</slot>
			</aside>
		</template>

		<div class="reveal">
			<div class="slides">
				<section id="slide-title">
					<h2>Vanilla and Framework-less</h2>
					<img src="assets/js.png" class="r-stretch">
					<p><a href="https://samhogy.co.uk">Sam Hogarth</a></p>
				</section>

				<section id="slide-intro">
					<h2>What this is not...</h2>
					<div class="r-stack">
						<p class="fragment fade-in-then-out">A rant about jQuery</p>
						<p class="fragment fade-in-then-out">A rant about Angular</p>
						<p class="fragment fade-in-then-out">A rant about React</p>
						<img class="fragment fade-in-then-out" src="assets/drevil.jpg">
					</div>
				</section>

				<section id="slide-legacy">
					<h2>Remember Me?</h2>
					<img src="assets/ie6.jpg">
				</section>

				<section id="slide-html5">
					<h2>Now We Have This</h2>
					<img src="assets/html5.png">
				</section>

				<section id="slide-dom">
					<h2>The DOM</h2>
					<div class="r-stack">
						<img src="assets/dom.png" class="fragment fade-in-then-out">
						<p class="fragment">React, Vue and Elm use a <strong>Virtual DOM</strong> to make minimal updates to the real DOM.<br />
							<span class="fragment supplementary fade-in-then-out">(At a cost of higher memory overhead)</span><br />
							<span class="fragment">There's also the <strong>Shadow DOM</strong> for encapsulation of nodes and styles. More later...</span>
						</p>
					</div>
				</section>

				<section id="slide-framework-definition">
					<h2>Framework</h2>
					<blockquote>A supporting structure around which something can be built</blockquote>
					<ul>
						<li>UI manipulation</li>
						<li>Networking</li>
						<li>Events</li>
						<li>Componentisation</li>
					</ul>
					<p>(amongst others)</p>
				</section>

				<section id="slide-ui-manipulation">
					<h2>UI Manipulation</h2>
					<div class="side-by-side">
						<pre>
							<code class="javascript" data-trim>
								$('.a-class-name')

								$(element).text('Hi')

								$(element).hasClass('foo')

								$(parent).append(element)
							</code>
						</pre>
						<pre class="fragment fade-right">
							<code class="javascript" data-trim>
								document.querySelectorAll('.a-class-name')

								element.textContent = 'Hi'

								element.classList.contains('foo')

								parent.appendChild(element)
							</code>
						</pre>
					</div>
				</section>

				<section id="slide-ui-animations">
					<h2>Animations Too!</h2>
					<div class="side-by-side">
						<pre>
							<code class="javascript" data-trim>
								$(element).fadeIn()
							</code>
						</pre>
						<pre class="fragment fade-right">
							<code class="javascript" data-trim>
								element.classList.add('show')
								element.classList.remove('hide')
							</code>
							<code class="css" data-trim>
								.show { transition: opacity 250ms; }
								.hide { opacity: 0; }
							</code>
						</pre>
					</div>
				</section>

				<section id="slide-template-strings-intro">
					<h2>Template Strings</h2>
					<pre>
						<code class="javascript" data-trim data-line-numbers="|4">
							const name = 'Sam'

							document.body.innerHTML = `
							<h1>Hi there, ${name}!</h1>
							<p>How are you today?</p>
							`
						</code>
					</pre>
				</section>

				<section id="slide-template-strings-gotcha">
					<h2>Mind You, Be Careful...</h2>
					<div class="r-stack">
						<pre class="fragment fade-in-then-out">
							<code class="javascript" data-trim data-line-numbers|>
								const names = ['Sam', 'Ian', 'Fraser']

								document.body.innerHTML = `
								<ul>
									${names.map(n => `<li>${n}</li>`)}
								</ul>
								`
							</code>
						</pre>
						<pre class="fragment fade-in-then-out">
							<code class="javascript" data-trim>
								<ul>
									<li>Sam</li>,
									<li>Ian</li>,
									<li>Fraser</li>
								</ul>
							</code>
						</pre>
						<pre class="fragment fade-in-then-out">
							<code class="javascript" data-trim data-line-numbers="5">
								const names = ['Sam', 'Ian', 'Fraser']

								document.body.innerHTML = `
								<ul>
									${names.map(n => `<li>${n}</li>`).join('')}
								</ul>
								`
							</code>
						</pre>
					</div>
				</section>

				<section id="slide-networking">
					<h2>Networking</h2>
					<div class="side-by-side">
						<pre>
							<code class="javascript" data-trim>
								$.getJSON(url, function(data) {
									...
								})
							</code>
						</pre>
						<pre class="fragment fade-right">
							<code class="javascript" data-trim>
								const response = await fetch(url)
								const json = await response.json()
							</code>
						</pre>
					</div>
					<sealion-aside class="fragment">
						<span slot="text" class="supplementary">Yes, I know this isn't <em>exactly</em> like-for-like</span>
					</sealion-aside>
				</section>

				<section id="slide-events">
					<h2>Events</h2>
					<p>You can define, listen to, and dispatch your own events</p>
					<pre>
						<code class="javascript" data-trim data-line-numbers="|1-3|5-7|9">
							const event = new CustomEvent('something-happened', {
								detail: someUsefulData
							})

							element.addEventListener('something-happened', e => {
								console.log(e.detail)
							})

							element.dispatchEvent(event)
						</code>
					</pre>
				</section>

				<section id="slide-web-components-intro">
					<h2>Web Components</h2>
					<p>Re-usable custom components that extend HTML</p>
					<ul class="horizontal-list tick-list fragment">
						<li>✔️ Firefox</li>
						<li>✔️ Safari</li>
						<li>✔️ Edge</li>
						<li>✔️ Chrome</li>
					</ul>
				</section>

				<section id="slide-webcomponents-define">
					<pre class="r-stretch">
						<code class="javascript" data-line-numbers="1|2-5|7-9|11-15|18" data-trim>
							class MyComponent extends HTMLElement {
								constructor() {
									super()
									// Set up DOM, styles, etc
								}

								// On DOM mount and unmount
								connectedCallback() { ... }
								disconnectedCallback() { ... }

								// Handle attribute changes
								attributeChangedCallback(name, oldValue, newValue) { ... }
								static get observedAttributes() {
									return [...]
								}
							}

							customElements.define('my-component', MyComponent)
						</code>
					</pre>
				</section>

				<section id="slide-webcomponents-templates-and-slots">
					<h2>A Worked Example...</h2>
					<sealion-aside class="fragment">
						<span slot="text" class="supplementary">Remember me? I'm a Web Component!</span>
					</sealion-aside>
					<pre class="fragment">
						<code class="javascript" data-trim data-line-numbers="|1|2|5">
							<template id="sealion">
								<style>/* ... */ </style>
								<aside class="sealion">
									<img src="assets/sealion.png">
									<slot name="text">Some text here</slot>
								</aside>
							</template>
						</code>
					</pre>
					<pre class="fragment">
						<code class="javascript" data-trim data-line-numbers="|2">
							<sealion-aside>
								<span slot="text">Remember me? I'm a Web Component!</span>
							</sealion-aside>
						</code>
					</pre>
				</section>

				<section id="slide-webcomponents-shadow-dom">
					<h2>Templates, Slots and Shadow DOMs</h2>
					<pre>
						<code class="javascript" data-trim data-line-numbers="|4-5|6-7">
							class SealionAside extends HTMLElement {
								constructor() {
									super()
									const template = document.getElementById('sealion')
									const templateContent = template.content
									const shadowRoot = this.attachShadow({ mode: 'open' })
									shadowRoot.appendChild(templateContent.cloneNode(true))
								}
							}
				
							customElements.define('sealion-aside', SealionAside)
						</code>
					</pre>
				</section>

				<section id="slide-theres-more">
					<h2>But Wait, There's More!</h2>
					<div class="side-by-side">
						<img src="assets/mays.png">
						<ul>
							<li>What about all the other stuff frameworks do?</li>
							<li class="fragment">I should still use a framework, right?</li>
							<li class="fragment">Well...</li>
						</ul>
					</div>
				</section>

				<section id="slide-conclusion">
					<img src="assets/it-depends.png">
				</section>
			</div>
		</div>

		<script>
			class SealionAside extends HTMLElement {
				constructor() {
					super()
					const template = document.getElementById('sealion')
					const templateContent = template.content
					const shadowRoot = this.attachShadow({ mode: 'open' })
					shadowRoot.appendChild(templateContent.cloneNode(true))
				}
			}

			customElements.define('sealion-aside', SealionAside)
		</script>
		<script src="dist/reveal.js"></script>
		<script src="plugin/highlight/highlight.js"></script>
		<script>
			Reveal.initialize({
				hash: true,
				plugins: [ RevealHighlight ]
			});
		</script>
	</body>
</html>
